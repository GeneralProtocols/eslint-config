module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true
    },
    extends: ['airbnb-base'],
    globals: {
        Atomics: 'readonly',
        BigInt: 'readonly',
        Buffer: 'readonly',
        process: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    rules: {
        // enforce consistent line breaks after opening and before closing array brackets
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/array-bracket-newline
        'array-bracket-newline': ['error', 'consistent'],

        // warn about consistent line breaks between array elements
        // warn instead of error to allow intentional deviations
        // https://eslint.org/docs/rules/array-element-newline
        'array-element-newline': ['warn', 'consistent'],

        // enforce spacing inside array brackets to be consistent with object brackets
        // https://eslint.org/docs/rules/array-bracket-spacing
        'array-bracket-spacing': ['error', 'always', { objectsInArrays: false, arraysInArrays: false }],

        // enforce allman brace style, disallow single line blocks
        // https://eslint.org/docs/rules/brace-style
        // https://eslint.org/docs/rules/no-unexpected-multiline
        // https://eslint.org/docs/rules/operator-linebreak
        'brace-style': ['error', 'allman'],
        'no-unexpected-multiline': ['off'],
        'operator-linebreak': ['error', 'before', { overrides: { '=': 'ignore' } }],

        // enforce consistent line breaks in function calls
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/function-call-argument-newline
        'function-call-argument-newline': ['off', 'consistent'],

        // require function expressions to have a name when it can't be inferred
        // https://eslint.org/docs/rules/func-names
        'func-names': ['error', 'as-needed'],

        // enforce intentional scoping for function declarations
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/func-style
        'func-style': ['error', 'expression'],

        // use tabs for indentation and use indentation in switch-cases for readability
        // https://eslint.org/docs/rules/indent
        // https://eslint.org/docs/rules/no-tabs
        // NOTE: This is set to warn due to incompatibility with typescript in some cases
        // https://github.com/typescript-eslint/typescript-eslint/issues/1824
        'indent': ['warn', "tab", { SwitchCase: 1 }],
        'no-tabs': ['off'],

        // allow multiple spaces for alignment
        // https://eslint.org/docs/rules/key-spacing
        'key-spacing': ['error', { mode: 'minimum' }],
        'no-multi-spaces': ['off'],

        // no spaces after if/for/while/catch/switch
        // https://eslint.org/docs/rules/keyword-spacing
        'keyword-spacing': ['error', { overrides: {
            if: { after: false },
            for: { after: false },
            while: { after: false },
            throw: { after: false },
            catch: { after: false },
            switch: { after: false },
        }}],

        // enforce position of line comments above the statement
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/line-comment-position
        'line-comment-position': ['error', {
            position: 'above',
            ignorePattern: '',
        }],

        // allow grouping of class properties in TypeScript
        // https://eslint.org/docs/rules/lines-between-class-members
        'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],

        // warn at very high nesting levels
        // https://eslint.org/docs/rules/max-depth
        'max-depth': ['warn', 5],

        // specify the maximum length of a line in your program
        // https://eslint.org/docs/rules/max-len
        'max-len': ['warn', 200, 4, {
            ignoreUrls: true,
            ignoreComments: false,
            ignoreRegExpLiterals: true,
            ignoreStrings: true,
            ignoreTemplateLiterals: true,
        }],

        // warn if max number of lines exceeds 250 (consistent with GitLab code quality reports)
        // turned off because of editor highlighting
        // https://eslint.org/docs/rules/max-lines
        'max-lines': ['off', {
            max: 250,
            skipBlankLines: true,
            skipComments: true,
        }],

        // warn if max function length exceeds 25 (consistent with GitLab code quality reports)
        // turned off because of editor highlighting
        // https://eslint.org/docs/rules/max-lines-per-function
        'max-lines-per-function': ['off', {
            max: 25,
            skipBlankLines: true,
            skipComments: true,
            IIFEs: true,
        }],

        // warn when the number of function parameters gets too large
        // https://eslint.org/docs/rules/max-params
        'max-params': ['warn', 5],

        // disallow multiple statements per line for readability
        // https://eslint.org/docs/rules/max-statements-per-line
        'max-statements-per-line': ['error', { max: 1 }],

        // disallow multiline ternaries for readability
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/multiline-ternary
        'multiline-ternary': ['warn', 'never'],

        // we prefer the readability of separated return statements
        // can be ignored if done intentionally
        // https://eslint.org/docs/rules/newline-before-return
        'newline-before-return': ['warn'],

        // enforces new line after each method call in the chain to make it
        // more readable and easy to maintain
        // https://eslint.org/docs/rules/newline-per-chained-call
        'newline-per-chained-call': ['error', { ignoreChainWithDepth: 2 }],

        // allow bitwise operators (e.g. for AnyHedge simulation)
        // https://eslint.org/docs/rules/no-bitwise
        'no-bitwise': ['off'],

        // allow the use of the continue statement because there is no clear
        // argument against them
        // https://eslint.org/docs/rules/no-continue
        'no-continue': ['off'],

        // discourage use of line comments on the same line as code
        // https://eslint.org/docs/rules/no-inline-comments
        'no-inline-comments': ['warn'],

        // we want to allow the use of for..in and for..of statements
        // https://eslint.org/docs/rules/no-restricted-syntax
        // https://eslint.org/docs/rules/guard-for-in
        'no-restricted-syntax': ['off'],
        'guard-for-in': ['off'],

        // enforce that an object is either on one line, or every property is
        // on a new line
        // https://eslint.org/docs/rules/object-curly-newline
        'object-curly-newline': ['error', { consistent: true }],

        // Disallow the use of Math.pow in favor of the ** operator for consistency
        // with other arithmetic operators
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/prefer-exponentiation-operator
        'prefer-exponentiation-operator': ['error'],

        // enforce consistency between anonymous and named functions
        // https://eslint.org/docs/rules/space-before-function-paren
        'space-before-function-paren': ['error', {
            anonymous: 'never',
            named: 'never',
            asyncArrow: 'always'
        }],


        // warn about unused variables to not break the pre-commit hooks
        // https://eslint.org/docs/rules/no-unused-vars
        'no-unused-vars': ['warn'],

	    // do not allow re-declaring names in nested namespaces.
        'no-shadow': ['error'],

        // for singleton classes it makes sense not to enforce this rule
        // https://eslint.org/docs/rules/class-methods-use-this
        'class-methods-use-this': ['off'],

        // encourages use of dot notation whenever possible
        // https://eslint.org/docs/rules/dot-notation
        'dot-notation': ['warn', { allowKeywords: true }],

        // warn about a maximum number of classes per file
        // https://eslint.org/docs/rules/max-classes-per-file
        'max-classes-per-file': ['warn', 1],

        // Disallow returning value in constructor
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/no-constructor-return
        'no-constructor-return': ['error'],

        // discourage unnecessary nested blocks
        // https://eslint.org/docs/rules/no-lone-blocks
        'no-lone-blocks': ['warn'],

        // allow use of multiple spaces
        // https://eslint.org/docs/rules/no-multi-spaces
        'no-multi-spaces': ['off'],

        // Sometimes you want to execute async calls in loops sequentially
        // https://eslint.org/docs/rules/no-await-in-loop
        'no-await-in-loop': ['off'],

        // disallow use of constant expressions in conditions
        // https://eslint.org/docs/rules/no-constant-condition
        'no-constant-condition': ['error'],

        // Disallow duplicate conditions in if-else-if chains
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/no-dupe-else-if
        'no-dupe-else-if': ['error'],

        // disallow reassigning imports
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/no-import-assign
        'no-import-assign': ['error'],

        // disallow returning values from setters
        // Airbnb wants to enable this, TODO: remove after enabled with airbnb
        // https://eslint.org/docs/rules/no-setter-return
        'no-setter-return': ['error'],

        // require explicit file extension when importing from files.
        // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/extensions.md
        'import/extensions': ['error', { 'js': 'always' }],

        // do not enforce any import ordering
        // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/order.md
        'import/order': ['off'],

        // do not require default export
        // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/prefer-default-export.md
        'import/prefer-default-export': ['off'],

        // allows shorthands on a case-by-case basis in order to maintain
        // consistency across sections of code with multiple statements.
        // https://eslint.org/docs/rules/object-shorthand
        'object-shorthand': ['off'],

        // callbacks can either be named functions or arrow functions
        'prefer-arrow-callback': ['error', {
            allowNamedFunctions: true,
        }],

        // Disable recently introduces rules that causes pipeline failures.
        // NOTE: We should re-evaluate each of these and consider updating code as needed.
        'no-promise-executor-return': 'off',
        'function-paren-newline': 'off',
    }
}
